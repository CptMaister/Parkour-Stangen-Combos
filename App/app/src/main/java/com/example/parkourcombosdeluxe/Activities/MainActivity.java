package com.example.parkourcombosdeluxe.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
//import android.widget.Toolbar;

import com.example.parkourcombosdeluxe.Data.AppDatabase;
import com.example.parkourcombosdeluxe.Data.CurrentCombo;
import com.example.parkourcombosdeluxe.Data.CurrentComboDao;
import com.example.parkourcombosdeluxe.Data.Favorite;
import com.example.parkourcombosdeluxe.Data.FavoriteDao;
import com.example.parkourcombosdeluxe.Data.History;
import com.example.parkourcombosdeluxe.Data.HistoryDao;
import com.example.parkourcombosdeluxe.Data.Move;
import com.example.parkourcombosdeluxe.Data.MoveDao;
import com.example.parkourcombosdeluxe.Data.MoveMinimal;
import com.example.parkourcombosdeluxe.Data.MoveSpotJoin;
import com.example.parkourcombosdeluxe.Data.MoveSpotJoinDao;
import com.example.parkourcombosdeluxe.Data.Profile;
import com.example.parkourcombosdeluxe.Data.ProfileDao;
import com.example.parkourcombosdeluxe.Data.ProfileMinimal;
import com.example.parkourcombosdeluxe.Data.Recommendation;
import com.example.parkourcombosdeluxe.Data.RecommendationProfileJoinDao;
import com.example.parkourcombosdeluxe.Data.Spot;
import com.example.parkourcombosdeluxe.Data.SpotDao;
import com.example.parkourcombosdeluxe.Data.Tipp;
import com.example.parkourcombosdeluxe.Data.TippDao;
import com.example.parkourcombosdeluxe.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.parkourcombosdeluxe.Data.Profile.*;
import static com.example.parkourcombosdeluxe.Helpers.ToastHelper.*;
import static java.lang.String.valueOf;


public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.textSpotStatus)
    TextView textSpotStatus;
    @BindView(R.id.layout_viewCombo)
    LinearLayout layout_viewCombo;
    @BindView(R.id.textViewRecommendation)
    TextView textViewRecommendation;

    @BindView(R.id.buttonCombo)
    Button buttonCombo;
    @BindView(R.id.buttonDone)
    ImageButton buttonDone;
    @BindView(R.id.buttonFavorite)
    ImageButton buttonFavorite;
    @BindView(R.id.buttonEmpty)
    ImageButton buttonEmpty;

    @BindView(R.id.editTextAmountMovesBottom)
    EditText editTextAmountMovesBottom;
    @BindView(R.id.editTextAmountMovesTop)
    EditText editTextAmountMovesTop;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;


    String currentSpotString;

    List<Tipp> allTipps;
    boolean toggledButtonDone = false;
    boolean toggledButtonFavorite = false;
    Random random = new Random();
    Toast myForcedToast;

    private int comboAmountMAX = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        //sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        //checkFirstRun();

        //sets saved language:
        changeLanguage();

        initiateViewsAndDBs();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(getResources().getString(R.string.sharedpref_main_just_created), true);
        editor.commit();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!(sharedPref.getBoolean(getResources().getString(R.string.sharedpref_main_just_created), true))) {
            recreate();
        }
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.buttonToolbarHistory) {
            startActivity(new Intent(MainActivity.this, HistoryActivity.class));
        } else if (id == R.id.buttonToolbarFavorites) {
            startActivity(new Intent(MainActivity.this, FavoriteActivity.class));
        } else if (id == R.id.buttonToolbarSpot) {
            startActivity(new Intent(MainActivity.this, SpotSelectActivity.class));
        } else if (id == R.id.buttonToolbarProfile) {
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
        } else if (id == R.id.action_games) {
            startActivity(new Intent(MainActivity.this, GamesActivity.class));
        } else if (id == R.id.action_moves) {
            startActivity(new Intent(MainActivity.this, MoveEditActivity.class));
        } else if (id == R.id.action_importExport) {
            //TODO: implement importExport
            toastMessageShort("implement Import/Export");
//            startActivity(new Intent(MainActivity.this, ExportImportActivity.class));
        } else if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
        } else if (id == R.id.action_help) {
            startActivity(new Intent(MainActivity.this, HelpActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_history) {
            startActivity(new Intent(MainActivity.this, HistoryActivity.class));
        } else if (id == R.id.nav_favorites) {
            startActivity(new Intent(MainActivity.this, FavoriteActivity.class));
        } else if (id == R.id.nav_spot_select) {
            startActivity(new Intent(MainActivity.this, SpotSelectActivity.class));
        } else if (id == R.id.nav_profile) {
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
        } else if (id == R.id.nav_games) {
            startActivity(new Intent(MainActivity.this, GamesActivity.class));
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void initiateViewsAndDBs() {
        //DataBase DAOs
        moveDao = AppDatabase.getInstance(this).moveDao();
        moveSpotJoinDao = AppDatabase.getInstance(this).moveSpotJoinDao();
        currentComboDao = AppDatabase.getInstance(this).currentComboDao();
        historyDao = AppDatabase.getInstance(this).historyDao();
        favoriteDao = AppDatabase.getInstance(this).favoriteDao();
        spotDao = AppDatabase.getInstance(this).spotDao();
        profileDao = AppDatabase.getInstance(this).profileDao();
        recommendationProfileJoinDao = AppDatabase.getInstance(this).recommendationProfileJoinDao();
        tippDao = AppDatabase.getInstance(this).tippDao();
        allTipps = tippDao.getAll();

        //Settings
        currentProfile = getCurrentProfile();
        setCurrentSpotIfThereAreAny();

        //other
        random = new Random();
        myForcedToast = Toast.makeText(getApplicationContext(), null, Toast.LENGTH_SHORT);

        //comboView Setup:
        setLayoutViewCombo();

        //status TextView
        if (currentProfile.getCurrentSpotId() == null) {
            currentSpotString = getResources().getString(R.string.main_status_no_spot_selected);
        } else {
            currentSpotString = currentSpot.getName();
        }
        String textSpotProfileStatusMessage = currentSpotString + ", " + currentProfile.getName();
        textSpotStatus.setText(textSpotProfileStatusMessage);

        //set lower left EditTexts
        editTextAmountMovesTop.setText(currentProfile.getAmountMovesBoundTop().toString());
        if (currentProfile.getAmountMovesBoundSetable()) {
            editTextAmountMovesBottom.setText(currentProfile.getAmountMovesBoundBottom().toString());
        } else {
            editTextAmountMovesBottom.setText(currentProfile.getAmountMovesBase().toString());
        }

        //Listeners
        initiateButtons();

        editTextAmountMovesTop.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                handleTextAmountMovesTop();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        editTextAmountMovesBottom.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                handleTextAmountMovesBottom();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
    }


    private void setLayoutViewCombo() {
        List<CurrentCombo> currentCombos = currentComboDao.getAll();
        if (currentCombos.size() < 1) {
            generateTipps();
        } else {
            for (int i = 0; i < currentCombos.size(); i++) {
                String nextMoveName;
                try {
                    nextMoveName = getLocalisedMoveName(moveDao.findByID(currentCombos.get(i).getMoveID()));
                } catch (NullPointerException e) {
                    nextMoveName = currentCombos.get(i).getMoveName();
                }
                layout_viewCombo.addView(createComboMoveTextViewFromString(nextMoveName));
            }
        }
    }

    private void generateTipps() {
        if (currentProfile.getTippsEnabled()) {
            int amountOfTippsToGenerate = 2;
            for (int i = 0; i < amountOfTippsToGenerate; i++) {
                Tipp nextTipp = allTipps.get(random.nextInt(allTipps.size()));
                String tippString = getLocalisedTippDescr(nextTipp);
                layout_viewCombo.addView(createTippTextViewFromString(tippString));
            }
        }
    }

    private void initiateButtons() {

        prepareGameMode(currentProfile.getCurrentGameMode());

        buttonCombo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickButtonCombo();
            }
        });
        buttonCombo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //TODO: implement gameMode change
                createPopupMenuGameMode(MainActivity.this);
                return false;
            }
        });


        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentComboDao.getAll().size() > 0) {
                    toggleButtonDone();
                }
            }
        });
        buttonFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentComboDao.getAll().size() > 0) {
                    toggleButtonFavorite();
                }
            }
        });
        buttonEmpty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickedButtonEmpty();
            }
        });
    }

    //from here used stuff, not automatically called by the activity
    private void prepareGameMode(int mode) {
        switch (mode) {
            case GAME_MODE_NORMAL:
                if (currentProfile.getAmountMovesBoundSetable()) {
                    editTextAmountMovesTop.setVisibility(View.VISIBLE);
                } else {
                    editTextAmountMovesTop.setVisibility(View.INVISIBLE);
                }
                editTextAmountMovesBottom.setVisibility(View.VISIBLE);

                buttonCombo.setBackground(getResources().getDrawable(R.drawable.stick_figure_active_reload));
                break;
            case GAME_MODE_BAGPACKING:
                editTextAmountMovesTop.setVisibility(View.INVISIBLE);
                editTextAmountMovesBottom.setVisibility(View.INVISIBLE);

                buttonCombo.setBackground(getResources().getDrawable(R.drawable.ic_icon_suitcase_slanted));
                break;
            case GAME_MODE_BAGPACKING_ROTATION:
                editTextAmountMovesTop.setVisibility(View.VISIBLE);
                editTextAmountMovesBottom.setVisibility(View.INVISIBLE);

                buttonCombo.setBackground(getResources().getDrawable(R.drawable.ic_icon_suitcase_slanted_rotation));
                break;
            default:
                break;
        }
    }


    public void createPopupMenuGameMode(Context popContext) {
        //creating a popup menu
        PopupMenu popup = new PopupMenu(popContext, buttonCombo);
        //inflating menu from xml resource
        popup.inflate(R.menu.floating_game_mode_select);
        //adding click listener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.floating_game_mode_standard:
                        changeGameMode(0);
                        break;
                    case R.id.floating_game_mode_bagpacking:
                        changeGameMode(1);
                        break;
                    case R.id.floating_game_mode_bagpacking_rotation:
                        changeGameMode(2);
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
        popup.show();
    }

    private void changeGameMode(int mode) {
        if (currentProfile.getCurrentGameMode() != mode) {
            currentProfile.setCurrentGameMode(mode);
            profileDao.updateProfiles(currentProfile);

            prepareGameMode(mode);
        }
    }


    private void clickButtonCombo() {
        if (noSpotForCurrentProfile) {
            createDialogSpotNone(MainActivity.this);
        } else {
            if (moveSpotJoinDao.getMoveIDsForSpot(currentSpot.getID()).size() < 1) {
                //TODO: write Querys that count amount of rows instead of getting all IDs and counting them
                createDialogSpotEmpty(MainActivity.this);
            } else {
                switch (currentProfile.getCurrentGameMode()) {
                    case GAME_MODE_NORMAL:
                        generateComboDefault();
                        break;
                    case GAME_MODE_BAGPACKING:
                        // bagPacking
                        generateRecommendationFromCurrentProfile();
                        generateMove();
                        break;
                    case GAME_MODE_BAGPACKING_ROTATION:
                        // bagPackingRotation
                        while (layout_viewCombo.getChildCount() >= currentProfile.getAmountMovesBoundTop()) {
                            popTopMove();
                        }
                        generateRecommendationFromCurrentProfile();
                        generateMove();
                        break;
                    default:
                        toastMessage(getResources().getString(R.string.toast_combo_generator_error));
                        generateComboDefault();
                }
            }
        }
    }

    public void createDialogSpotNone(Context context) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_main_spot_none, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        Button buttonCopySpot = (Button) layout.findViewById(R.id.spot_none_dialog_button_spot_copy);
        Button buttonSpotMenu = (Button) layout.findViewById(R.id.spot_none_dialog_button_spot_menu);
        Button buttonCancel = (Button) layout.findViewById(R.id.spot_none_dialog_button_cancel);

        //TODO: YEET toast
        buttonCopySpot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                createDialogSpotCopy(MainActivity.this);
            }
        });

        buttonSpotMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                startActivity(new Intent(MainActivity.this, SpotSelectActivity.class));
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }

    public void createDialogSpotEmpty(Context context) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_main_spot_empty, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        Button buttonSpotMenu = (Button) layout.findViewById(R.id.spot_empty_dialog_button_spot_menu);
        Button buttonCancel = (Button) layout.findViewById(R.id.spot_empty_dialog_button_cancel);

        buttonSpotMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                startActivity(new Intent(MainActivity.this, SpotSelectActivity.class));
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }


    public void createDialogSpotCopy(Context context) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_main_spot_copy, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        LinearLayout layoutSpotCopy = (LinearLayout) layout.findViewById(R.id.spot_copy_dialog_linearlayout);
        Button buttonCancel = (Button) layout.findViewById(R.id.spot_copy_dialog_button_cancel);

        //list all profiles with their spots
        List<ProfileMinimal> allProfilesMinimal = profileDao.getAllMinimal();
        for (int i = 0; i < profileDao.getAllIDs().size(); i++) {
            ProfileMinimal nextProfile = allProfilesMinimal.get(i);
            layoutSpotCopy.addView(createCopySpotTextViewFromString(nextProfile.getName(), true));

            List<Spot> nextSpots = spotDao.getAllByProfileID(nextProfile.getID());
            if (nextSpots.size() > 0) {
                for (int j = 0; j < nextSpots.size(); j++) {
                    final Spot nextSpot = nextSpots.get(j);
                    TextView nextSpotView = createCopySpotTextViewFromString(nextSpot.getName(), false);
                    nextSpotView.setClickable(true);
                    nextSpotView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            copySpotAndUpdateCurrentProfile(nextSpot.getID());

                            alertDialog.dismiss();
                            recreate();
                        }
                    });
                    layoutSpotCopy.addView(nextSpotView);
                }
            } else {
                TextView noSpot = createCopySpotTextViewFromString(getResources().getString(R.string.dialog_main_spot_copy_no_spots_on_profile), false);
                noSpot.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.scrollview_descr));
                layoutSpotCopy.addView(noSpot);
            }
        }

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }

    private void copySpotAndUpdateCurrentProfile(int copySpotID) {
        Spot copySpot = spotDao.findByID(copySpotID);
        spotDao.insertAll(new Spot(currentProfile.getID(), copySpot.getName() + " " + getResources().getString(R.string.plain_copy)));
        copyMovesFromSpotToEmptySpot(spotDao.findByID(copySpotID).getID(), spotDao.getAll().get(spotDao.getAll().size() - 1).getID());

        //set newest spot as current spot
        currentSpot = spotDao.getAll().get(spotDao.getAll().size() - 1);
        currentProfile.setCurrentSpotId(currentSpot.getID());
        profileDao.updateProfiles(currentProfile);

        noSpotForCurrentProfile = false;
        toastMessageShort(getResources().getString(R.string.toast_spot_copy_confirm));
    }


    private void generateComboDefault() {
        generateCombo();
        addHistory();
        generateRecommendationFromCurrentProfile();
        resetButtonDone();
        resetButtonFavorite();
    }

    private void generateCombo() {
        layout_viewCombo.removeAllViews();
        currentComboDao.deleteAll();
        List<MoveMinimal> moves = AppDatabase.getInstance(this).moveSpotJoinDao().getMovesForSpotMinimal(currentSpot.getID());
        int amountMoves = getRandomIntegerWithinBoundsWithBase(currentProfile.getAmountMovesBase(), currentProfile.getAmountMovesFuzzMinus(), currentProfile.getAmountMovesFuzzPlus());
        for (int i = 0; i < amountMoves; i++) {
            layout_viewCombo.addView(getRandomTextViewMovesMinimalAddHistory(moves));
        }
    }

    private void generateMove() {
        List<MoveMinimal> moves = AppDatabase.getInstance(this).moveSpotJoinDao().getMovesForSpotMinimal(currentSpot.getID());
        View addView = getRandomTextViewMovesMinimalAddHistory(moves);
        layout_viewCombo.addView(addView);
        if (currentProfile.getCurrentGameMode() == 1) {
            addView.requestFocus();
        }
    }

    private View getRandomTextViewMovesAddHistory(List<Move> moves) {
        Move randomMove = moves.get(random.nextInt(moves.size()));
        String moveName = getLocalisedMoveName(randomMove); // randomMove.getEnglishName();
        CurrentCombo nextMove = new CurrentCombo(randomMove.getID(), moveName);
        currentComboDao.insertAll(nextMove);
        return createComboMoveTextViewFromString(moveName);
    }

    private View getRandomTextViewMovesMinimalAddHistory(List<MoveMinimal> moves) {
        MoveMinimal randomMove = moves.get(random.nextInt(moves.size()));
        String moveName = getLocalisedMoveMinimalName(randomMove); // randomMove.getEnglishName();
        CurrentCombo nextMove = new CurrentCombo(randomMove.getID(), moveName);
        currentComboDao.insertAll(nextMove);
        return createComboMoveTextViewFromString(moveName);
    }

    private View getRandomTextViewMoves(List<Move> moves) {
        return createComboMoveTextViewFromString(getLocalisedMoveName(moves.get(random.nextInt(moves.size()))));
    }

    private TextView createComboMoveTextViewFromString(String moveString) {
        TextView child = new TextView(MainActivity.this);
        child.setWidth(layout_viewCombo.getWidth());
        child.setText(moveString);
        child.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        /*TypedValue outValue = new TypedValue();
        MainActivity.this.getTheme().resolveAttribute(R.attr.colorPrimary,
                outValue, true);
        final int themeColorAccent = outValue.resourceId;
        //or outValue.resourceId

        child.setTextColor(getResources().getColor(themeColorAccent));*/


        /*Resources.Theme themes = getTheme();
        TypedValue storedValueInTheme = new TypedValue();
        if (themes.resolveAttribute(R.attr.colorAccent, storedValueInTheme, true)) {
            child.setTextColor(storedValueInTheme.data);
        }*/

        //int colorID = getAttributeColor(MainActivity.this, R.attr.colorAccent);
        //child.setTextColor(getResources().getColor(R.attr.colorAccent));

        //child.setTextColor(getResources().getColor(R.color.colorTextLight));
        //TODO child.setTextColor(getResources().getColor(R.color.textColor));

        /*TypedValue typedValue = new TypedValue();
        Resources.Theme theme = MainActivity.this.getTheme();
        theme.resolveAttribute(R.attr., typedValue, true);
        @ColorInt int color = typedValue.data;*/

        child.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimension(R.dimen.main_text_combo_normal));
        child.setTextIsSelectable(true);

        if (currentProfile.getCurrentGameMode() == 1) {
            child.setFocusable(true);
            child.setFocusableInTouchMode(true);
        }

        return child;
    }

    private TextView createTippTextViewFromString(String tippString) {
        TextView child = new TextView(MainActivity.this);
        child.setWidth(layout_viewCombo.getWidth());
        child.setText(tippString);
        child.setPadding(100, 60, 60, 0);
        //child.setTextColor(getResources().getColor(R.color.colorTextLight));
        //TODO child.setTextColor(getResources().getColor(R.color.textColor));

        /*TypedValue typedValue = new TypedValue();
        Resources.Theme theme = MainActivity.this.getTheme();
        theme.resolveAttribute(R.attr., typedValue, true);
        @ColorInt int color = typedValue.data;*/

        child.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimension(R.dimen.scrollview_descr));
        child.setTextIsSelectable(true);

        return child;
    }

    private TextView createCopySpotTextViewFromString(String string, boolean profileView) {
        TextView child = new TextView(MainActivity.this);
        //child.setWidth(layout_viewCombo.getWidth());
        child.setText(string);
        if (profileView) {
            child.setPadding(0, 0, 0, 0);
            child.setTypeface(Typeface.DEFAULT_BOLD);
            child.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    getResources().getDimension(R.dimen.scrollview_title));
        } else {
            child.setPadding(60, 5, 0, 5);
            child.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    getResources().getDimension(R.dimen.scrollview_title_big));
        }

        return child;
    }


    private void generateRecommendationFromCurrentProfile() {
        Integer randomWeightLowerLimit = random.nextInt(200);
        List<Recommendation> recommendationsFromCurrentProfileWithMinWeight = recommendationProfileJoinDao.getRecommendationsForProfileWithMinWeight(currentProfile.getID(), randomWeightLowerLimit);
        if (recommendationsFromCurrentProfileWithMinWeight.size() < 1) {
            textViewRecommendation.setText("");
        } else {
            Integer randomRecomm = random.nextInt(recommendationsFromCurrentProfileWithMinWeight.size());
            String randomRecommendationName = getLocalisedRecommendationDescr(recommendationsFromCurrentProfileWithMinWeight.get(randomRecomm));
            String message = getResources().getString(R.string.main_recommendation_translation_colon) + " " + randomRecommendationName;
            textViewRecommendation.setText(message);
        }
    }


    private void addHistory() {
        String comboMovesContentSeparator = " -> ";
        String historyJoined = createStringFromComboMoves(currentComboDao.getAll(), comboMovesContentSeparator);
        historyDao.insertAll(new History(historyJoined, false, false, getCurrentDateString(), currentProfile.getID(), currentProfile.getName()));
    }

    private String createStringFromComboMoves(List<CurrentCombo> list, String separator) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i).getMoveName());
            sb.append(separator);
        }
        return sb.toString().substring(0, sb.toString().length() - separator.length());

    }

    private void popTopMove() {
        layout_viewCombo.removeViewAt(0);
    }


    //activate the visual and set bool
    private void activateButtonDone() {
        buttonDone.setImageDrawable(getDrawable(R.drawable.icon_check_fill_green));
        //buttonDone.setColorFilter(getResources().getColor(R.color.colorGreenCheck), PorterDuff.Mode.SRC_ATOP);
        toggledButtonDone = true;
    }

    //clear the visual and set bool
    private void clearButtonDone() {
        buttonDone.setImageDrawable(getDrawable(R.drawable.icon_check_empty));
        //buttonDone.setColorFilter(getResources().getColor(R.color.colorButtonLight), PorterDuff.Mode.SRC_ATOP);
        toggledButtonDone = false;
    }


    //when clicking the comboButton, for when animations are added?
    private void resetButtonDone() {
        clearButtonDone();
    }

    //when clicking the Button
    private void toggleButtonDone() {
        if (toggledButtonDone) {
            clearButtonDone();
        } else {
            activateButtonDone();
        }
        toggleDoneOnCurrentComboHistory();
    }

    private void toggleDoneOnCurrentComboHistory() {
        List<History> historys = AppDatabase.getInstance(this).historyDao().getAll();
        if (historys.size() > 0) {
            History newestHistory = historys.get(historys.size() - 1);
            historyDao.delete(newestHistory);
            if (toggledButtonDone && doableGameMode(currentProfile.getCurrentGameMode())) {
                newestHistory.setDone(true);
                currentSpot.addToCombosDoneCount(1);
            } else {
                newestHistory.setDone(false);
                currentSpot.addToCombosDoneCount(-1);
            }
            spotDao.updateSpots(currentSpot);
            historyDao.insertAll(newestHistory);
        }
    }


    private void activateButtonFavorite() {
        buttonFavorite.setImageDrawable(getDrawable(R.drawable.icon_heart_fill_pink));
        //buttonFavorite.setColorFilter(getResources().getColor(R.color.colorRedFavorite), PorterDuff.Mode.SRC_ATOP);
        toggledButtonFavorite = true;
    }

    private void clearButtonFavorite() {
        buttonFavorite.setImageDrawable(getDrawable(R.drawable.icon_heart_empty));
        //buttonFavorite.setColorFilter(getResources().getColor(R.color.colorButtonLight), PorterDuff.Mode.SRC_ATOP);
        toggledButtonFavorite = false;
    }

    private void resetButtonFavorite() {
        clearButtonFavorite();
    }

    private void toggleButtonFavorite() {
        if (toggledButtonFavorite) {
            clearButtonFavorite();
        } else {
            activateButtonFavorite();
        }
        toggleFavoriteOnCurrentCombo();
        toggleFavoriteOnCurrentComboHistory();
    }

    private void toggleFavoriteOnCurrentCombo() {
        if (toggledButtonFavorite) {
            Favorite newFavorite = new Favorite("", createStringFromComboMoves(currentComboDao.getAll(), " -> "), currentProfile.getID(), currentProfile.getName());
            favoriteDao.insertAll(newFavorite);
            Favorite titleChangeFavorite = favoriteDao.findByID(favoriteDao.getNewestID());
            titleChangeFavorite.setTitle(getResources().getString(R.string.plain_combo) + " " + titleChangeFavorite.getID());
            favoriteDao.updateFavorites(titleChangeFavorite);
        } else {
            List<Favorite> favorites = AppDatabase.getInstance(this).favoriteDao().getAll();
            if (favorites.size() > 0) {
                favoriteDao.delete(favorites.get(favorites.size() - 1));
            }
        }
    }

    private void toggleFavoriteOnCurrentComboHistory() {
        //TODO make better
        List<History> historys = AppDatabase.getInstance(this).historyDao().getAll();
        if (historys.size() > 0) {
            History newestHistory = historys.get(historys.size() - 1);
            historyDao.delete(newestHistory);
            newestHistory.setFav(toggledButtonFavorite);
            historyDao.insertAll(newestHistory);
        }
    }


    private void clickedButtonEmpty() {
        resetButtonDone();
        resetButtonFavorite();
        currentComboDao.deleteAll();
        layout_viewCombo.removeAllViews();
        generateTipps();
    }


    // from here set and get private values

    //Booleans:
    private Boolean doableGameMode(Integer comboGameModeIn) {
        return (comboGameModeIn == 0);
    }

    public boolean getToggledButtonDone() {
        return this.toggledButtonDone;
    }

    public void setToggledButtonDone(boolean toggledButtonDone) {
        this.toggledButtonDone = toggledButtonDone;
    }

    public boolean getToggledButtonFavorite() {
        return this.toggledButtonFavorite;
    }

    public void setToggledButtonFavorite(boolean toggledButtonFavorite) {
        this.toggledButtonFavorite = toggledButtonFavorite;
    }

    public void handleTextAmountMovesTop() {
        int input;
        int entry;
        if (checkTextAmountMovesMax(editTextAmountMovesTop)) {
            input = Integer.parseInt(editTextAmountMovesTop.getText().toString());
            if (input > 0) {
                entry = input;
            } else {
                entry = 1;
            }
        } else if (editTextAmountMovesTop.getText().length() == 0) {
            entry = 1;
        } else {
            toastMessage(getString(R.string.error_message_set_combo_amount));

            editTextAmountMovesTop.setText(currentProfile.getAmountMovesBoundTop().toString());
            if (Integer.parseInt(editTextAmountMovesBottom.getText().toString()) > Integer.parseInt(editTextAmountMovesTop.getText().toString())) {
                editTextAmountMovesBottom.setText(editTextAmountMovesTop.getText());
                handleTextAmountMovesBottom();
            }
            return;
        }

        currentProfile.setAmountMovesBoundTop(entry);
        profileDao.updateProfiles(currentProfile);

    }

    public void handleTextAmountMovesBottom() {
        int input;
        int entry = 7;
        if (checkTextAmountMovesMax(editTextAmountMovesBottom)) {
            input = Integer.parseInt(editTextAmountMovesBottom.getText().toString());
            if (input > 0) {
                entry = input;
            } else {
                entry = 1;
            }
        } else if (editTextAmountMovesBottom.getText().length() == 0) {
            entry = 1;
        } else {
            toastMessage(getString(R.string.error_message_set_combo_amount));

            if (currentProfile.getAmountMovesBoundSetable()) {
                editTextAmountMovesBottom.setText(currentProfile.getAmountMovesBoundBottom().toString());
            } else {
                editTextAmountMovesBottom.setText(currentProfile.getAmountMovesBase().toString());
            }
            if (Integer.parseInt(editTextAmountMovesBottom.getText().toString()) > Integer.parseInt(editTextAmountMovesTop.getText().toString())) {
                editTextAmountMovesTop.setText(editTextAmountMovesBottom.getText());
                handleTextAmountMovesTop();
            }
            return;
        }
        currentProfile.setAmountMovesBase(entry);
        profileDao.updateProfiles(currentProfile);
    }


    private boolean checkTextAmountMovesMax(EditText checkView) {
        try {
            int check = Integer.parseInt(checkView.getText().toString());
            return check <= comboAmountMAX;
        } catch (NumberFormatException e) {
            return false;
        }
    }


    private int getRandomIntegerWithinBoundsWithBase(int base, int lowerTolerance, int upperTolerance) {
        int output = base - lowerTolerance + random.nextInt(upperTolerance + lowerTolerance + 1);
        return Math.max(output, 1);
    }

    public String getCurrentDateString() {
        try {
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date now = calendar.getTime();
            return sdf.format(now);
        } catch (Exception e) {
        }
        return "";
    }



    /*public Profile getCurrentProfile() {
        Profile returnProfile;
        try {
            returnProfile = AppDatabase.getInstance(this).profileDao().findByID(PreferenceManager.getDefaultSharedPreferences(this).getInt(getResources().getString(R.string.sharedpref_current_profileid), 1));
            String checkName = returnProfile.getName();
            //String checkDescription = returnProfile.getDescription();
            //int checkID = returnProfile.getID();
            //int checkSpotID = returnProfile.getCurrentSpotId();
            //int checkGameMode = returnProfile.getCurrentGameMode();
        } catch (NullPointerException e) {
            returnProfile = AppDatabase.getInstance(this).profileDao().getAll().get(0);
            SharedPreferences.Editor editorr = PreferenceManager.getDefaultSharedPreferences(this).edit();
            editorr.putInt(getResources().getString(R.string.sharedpref_current_profileid), returnProfile.getID());
            editorr.commit();
        }
        return returnProfile;
    }*/

}