package com.example.parkourcombosdeluxe.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.parkourcombosdeluxe.Adapters.MoveEditAdapter;
import com.example.parkourcombosdeluxe.Data.AppDatabase;
import com.example.parkourcombosdeluxe.Data.MoveSpotJoinDao;
import com.example.parkourcombosdeluxe.Data.MoveSpotTypeNeeded;
import com.example.parkourcombosdeluxe.Data.MoveSpotTypeNeededDao;
import com.example.parkourcombosdeluxe.Data.MoveStyleOrDifficulty;
import com.example.parkourcombosdeluxe.Data.MoveStyleOrDifficultyDao;
import com.example.parkourcombosdeluxe.Data.MovementType;
import com.example.parkourcombosdeluxe.Data.MovementTypeDao;
import com.example.parkourcombosdeluxe.Data.Profile;
import com.example.parkourcombosdeluxe.Data.ProfileDao;
import com.example.parkourcombosdeluxe.Data.Move;
import com.example.parkourcombosdeluxe.Data.MoveDao;
import com.example.parkourcombosdeluxe.ItemsForRecyclerView.MoveEditItem;
import com.example.parkourcombosdeluxe.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.parkourcombosdeluxe.Data.Profile.*;

public class MoveEditActivity extends BaseActivity {

    /*@BindView(R.id.layout_MoveEditScroll)
    RecyclerView.Recycler recyclerView;*/
    @BindView(R.id.moveEditStatusText)
    TextView moveEditStatusText;
    @BindView(R.id.moveEditButtonCardColorCoding)
    ImageButton moveEditButtonCardColorCoding;



    public final int NO_DOUBLE_NAMES = 0;
    public final int DOUBLE_ENGLISH_NAME = 1;
    public final int DOUBLE_GERMAN_NAME = 2;

    public final int DIFFICULTY = 0;
    public final int SPOT_TYPE = 1;
    public final int MOVEMENT_TYPE = 2;

    int buttonDifficultyState;
    int buttonSpotTypeState;
    int buttonMovementTypeState;

    boolean setDifficulty = false;
    boolean setSpotType = false;
    boolean setMovementType = false;

    boolean userHasBeenWarnedDoubleName = false;
    boolean entryHasBeenChanged = false;

    private ArrayList<MoveEditItem> mMoveEditList;

    private RecyclerView mRecyclerView;
    private MoveEditAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    List<Move> allActiveMoves;
    List<Boolean> actionModeCheckStates;
    public static boolean actionModeActive = false;
    String currentLanguage;

    private ActionMode mActionMode;
    private Menu menu;
    String tempMoveNameFromDiscard = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_move_edit);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tempMoveNameFromDiscard = getResources().getString(R.string.move_name);

        //DBs
        moveDao = getDBs.moveDao();
        profileDao = getDBs.profileDao();
        moveSpotJoinDao = getDBs.moveSpotJoinDao();
        moveStyleOrDifficultyDao = getDBs.moveStyleOrDifficultyDao();
        moveSpotTypeNeededDao = getDBs.moveSpotTypeNeededDao();
        movementTypeDao = getDBs.movementTypeDao();

        allActiveMoves = moveDao.getAllActive();
        currentLanguage = sharedPref.getString(getResources().getString(R.string.sharedpref_current_selected_language), "en");

        if (sharedPref.getBoolean(getResources().getString(R.string.sharedpref_move_card_color_coding), false)) {
            setColorCodingButtonImage();
            moveEditButtonCardColorCoding.setVisibility(View.VISIBLE);
            moveEditButtonCardColorCoding.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    toggleColorCodingButton();
                }
            });
        } else {
            moveEditButtonCardColorCoding.setVisibility(View.INVISIBLE);
        }

        generateMoveEdit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_move_edit, menu);
        this.menu = menu;
        updateSortIcon();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_move_edit_help:
                createDialogHelpBasic(MoveEditActivity.this, getResources().getString(R.string.move_edit_dialog_help_title), getResources().getString(R.string.move_edit_dialog_help_text));
                break;
            case R.id.action_move_edit_add:
                createDialogAddMove(MoveEditActivity.this);
                break;
            case R.id.action_move_edit_sort:
                createDialogSortMove(MoveEditActivity.this);
                break;
            case R.id.action_move_edit_archive:
                //TODO: Implement Move Archive Activity
                toastMessage("Implement Move Archive Activity");
                //startActivity(new Intent(MoveEditActivity.this, MoveArchiveActivity.class));
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void createPopupMenu(Context popContext, View view, final int position) {
        //creating a popup menu
        PopupMenu popup = new PopupMenu(popContext, view);
        //inflating menu from xml resource
        popup.inflate(R.menu.floating_move_edit);
        //adding click listener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.floating_move_edit_edit:
                        createDialogEditMove(position, MoveEditActivity.this);
                        break;
                    case R.id.floating_move_edit_toggle:
                        createDialogToggleMoveInSpots(position, MoveEditActivity.this);
                        toastMessage("toggle" + String.valueOf(position));
                        break;
                    case R.id.floating_move_edit_archive:
                        createDialogArchiveMove(position, MoveEditActivity.this);
                        break;
                    case R.id.floating_move_edit_color:
                        toastMessage("color" + String.valueOf(position) + mMoveEditList.get(position).getIconColor());
                        break;
                    case R.id.floating_move_edit_delete:
                        createDialogDeleteMove(position, MoveEditActivity.this);
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
        popup.show();
    }

    private void generateMoveEdit() {
        mMoveEditList = new ArrayList<>();
        updateMoveEditStatusText();

        if (allActiveMoves.size() > 0) {
            //CAREFUL: i should be one smaller than moves.size() in the last loop, as i is used in moves.get(i), where counting starts from 0
            for (int i = allActiveMoves.size() - 1; i >= 0; i--) {
                mMoveEditList.add(new MoveEditItem(allActiveMoves.get(i)));
            }
            sortMoveList();
            buildRecyclerView();
        }
    }


    private void buildRecyclerView() {
        mRecyclerView = findViewById(R.id.moveEditRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new MoveEditAdapter(mMoveEditList, MoveEditActivity.this);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        registerForContextMenu(mRecyclerView);

        mAdapter.setItemOnClickListener(new MoveEditAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position) {
                if (actionModeActive) {
                    boolean oldCheckedState = actionModeCheckStates.get(position);
                    mMoveEditList.get(position).setActionModeChecked(!oldCheckedState);
                    actionModeCheckStates.set(position, !oldCheckedState);
                    mAdapter.notifyItemChanged(position);
                } else {
                    createPopupMenu(MoveEditActivity.this, mRecyclerView.findViewHolderForAdapterPosition(position).itemView, position);
                }
            }

            @Override
            public void onFavoriteClicked(int position) {
                toggleButtonFavorite(position);
            }
        });

        mAdapter.setItemOnLongClickListener(new MoveEditAdapter.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClicked(int position) {
                if (mActionMode != null) {
                    return false;
                }

                //reset all checkboxes and the boolean list
                actionModeCheckStates = new ArrayList<Boolean>(Collections.nCopies(mMoveEditList.size(), false));
                deselectAllMoves();

                // select the item that was longclicked
                mMoveEditList.get(position).setActionModeChecked(true);
                actionModeCheckStates.set(position, true);

                mActionMode = startSupportActionMode(mActionModeCallback);
                return false;
            }
        });
    }

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_move_edit_actionmode, menu);
            mode.setTitle("Configure");

            actionModeActive = true;
            updateLongPressLayoutOnCards();
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.actionmode_move_edit_multiselect:
                    toastMessage("multi");
                    return true;
                case R.id.actionmode_move_edit_sort:
                    toastMessage("sort");
                    return true;
                case R.id.actionmode_move_edit_properties:
                    toastMessage("properties");
                    return true;
                case R.id.actionmode_move_edit_delete:
                    deleteSelectedMoves();
                    toastMessage("delete");
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
            actionModeActive = false;
            updateLongPressLayoutOnCards();
        }
    };

    private void updateLongPressLayoutOnCards() {
        mAdapter.notifyDataSetChanged();
    }

    private void deselectAllMoves() {
        for (MoveEditItem move : mMoveEditList) {
            move.setActionModeChecked(false);
        }
    }

    private void deleteSelectedMoves() {
        for (int i = actionModeCheckStates.size() - 1; i >= 0; i--) {
            if (actionModeCheckStates.get(i)) {
                moveDao.delete(mMoveEditList.get(i).getMove());
                mMoveEditList.remove(i);
                mAdapter.notifyItemRemoved(i);
            }
        }
        updateMoveEditStatusText();

        if (mActionMode != null)
        {
            mActionMode.finish();
        }
    }









    private void createDialogSortMove(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_sort_move_edit, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        RadioGroup radioGroupMode = (RadioGroup) layout.findViewById(R.id.radioGroupMoveEditMode);
        RadioGroup radioGroupDirection = (RadioGroup) layout.findViewById(R.id.radioGroupMoveEditDirection);

        //set current checked:
        RadioButton currentRadioButtonMode;
        switch (currentProfile.getSortByMoveEditMode()) {
            case SORT_BY_DATE:
                currentRadioButtonMode = (RadioButton) radioGroupMode.findViewById(R.id.sort_move_edit_sort_by_date);
                break;
            case SORT_BY_NAME:
                currentRadioButtonMode = (RadioButton) radioGroupMode.findViewById(R.id.sort_move_edit_sort_by_name);
                break;
            case SORT_BY_FAVORITE:
                currentRadioButtonMode = (RadioButton) radioGroupMode.findViewById(R.id.sort_move_edit_sort_by_favorite);
                break;
            case SORT_BY_DIFFICULTY:
                currentRadioButtonMode = (RadioButton) radioGroupMode.findViewById(R.id.sort_move_edit_sort_by_difficulty);
                break;
            case SORT_BY_SPOTTYPE:
                currentRadioButtonMode = (RadioButton) radioGroupMode.findViewById(R.id.sort_move_edit_sort_by_spot_type);
                break;
            default:
                currentRadioButtonMode = (RadioButton) radioGroupMode.findViewById(R.id.sort_move_edit_sort_by_date);
                break;
        }
        currentRadioButtonMode.setChecked(true);

        RadioButton currentRadioButtonDirection;
        if (currentProfile.getSortMoveEditAscending()) {
            currentRadioButtonDirection = (RadioButton) radioGroupDirection.findViewById(R.id.sort_move_edit_sort_ascending);
        } else {
            currentRadioButtonDirection = (RadioButton) radioGroupDirection.findViewById(R.id.sort_move_edit_sort_descending);
        }
        currentRadioButtonDirection.setChecked(true);


        //handle selection here
        radioGroupMode.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int savedSortMode = currentProfile.getSortByMoveEditMode();
                switch (checkedId) {
                    case R.id.sort_move_edit_sort_by_date:
                        currentProfile.setSortByMoveEditMode(SORT_BY_DATE);
                        break;
                    case R.id.sort_move_edit_sort_by_name:
                        currentProfile.setSortByMoveEditMode(SORT_BY_NAME);
                        break;
                    case R.id.sort_move_edit_sort_by_favorite:
                        currentProfile.setSortByMoveEditMode(SORT_BY_FAVORITE);
                        break;
                    case R.id.sort_move_edit_sort_by_difficulty:
                        currentProfile.setSortByMoveEditMode(SORT_BY_DIFFICULTY);
                        break;
                    case R.id.sort_move_edit_sort_by_spot_type:
                        currentProfile.setSortByMoveEditMode(SORT_BY_SPOTTYPE);
                        break;
                    default:
                        break;
                }
                if (savedSortMode != currentProfile.getSortByMoveEditMode() || entryHasBeenChanged) {
                    profileDao.updateProfiles(currentProfile);

                    alertDialog.dismiss();
                    MoveEditActivity.this.recreate();
                }
            }
        });

        radioGroupDirection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                boolean savedSortAscending = currentProfile.getSortMoveEditAscending();
                switch (checkedId) {
                    case R.id.sort_move_edit_sort_ascending:
                        currentProfile.setSortMoveEditAscending(true);
                        break;
                    case R.id.sort_move_edit_sort_descending:
                        currentProfile.setSortMoveEditAscending(false);
                        break;
                    default:
                        currentProfile.setSortMoveEditAscending(true);
                        break;
                }
                if (savedSortAscending != currentProfile.getSortMoveEditAscending()) {
                    profileDao.updateProfiles(currentProfile);

                    alertDialog.dismiss();
                    MoveEditActivity.this.recreate();
                }
            }
        });

        alertDialog.show();
    }


    private void createDialogAddMove(final Context context) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;

        //get all elements of the layout
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_move_add, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        LinearLayout layoutMoveAddTexts = (LinearLayout) layout.findViewById(R.id.move_add_dialog_linearlayout);
        TextView addTitle = (TextView) layout.findViewById(R.id.move_add_dialog_title);
        final EditText addMoveNameEnglish = (EditText) layout.findViewById(R.id.move_add_name_english);
        final EditText addMoveNameGerman = (EditText) layout.findViewById(R.id.move_add_name_german);
        final Button buttonDifficulty = (Button) layout.findViewById(R.id.move_add_dialog_button_difficulty);
        final Button buttonSpotType = (Button) layout.findViewById(R.id.move_add_dialog_button_spot_type);
        final Button buttonMovementType = (Button) layout.findViewById(R.id.move_add_dialog_button_movement_type);
        Button buttonSave = (Button) layout.findViewById(R.id.move_add_dialog_button_save);
        Button buttonCancel = (Button) layout.findViewById(R.id.move_add_dialog_button_cancel);

        buttonMovementType.setText(getLocalisedMovementTypeDescr(1, currentLanguage));
        buttonMovementTypeState = 1;

        boolean englishEditTextSet = false;
        boolean germanEditTextSet = false;

        //find out which languages to add:
        switch (currentLanguage) {
            case "en":
                addMoveNameEnglish.setVisibility(View.VISIBLE);
                englishEditTextSet = true;
                break;
            case "de":
                addMoveNameGerman.setVisibility(View.VISIBLE);
                germanEditTextSet = true;
                break;
            default:
                addMoveNameEnglish.setVisibility(View.VISIBLE);
                break;
        }

        //TODO: test if this is even working at all, once settings are implemented far enough
        if (!englishEditTextSet && currentProfile.getEditEnglishNames()) {
            addMoveNameEnglish.setVisibility(View.VISIBLE);
            englishEditTextSet = true;
        }
        if (!germanEditTextSet && currentProfile.getEditGermanNames()) {
            addMoveNameGerman.setVisibility(View.VISIBLE);
            germanEditTextSet = true;
        }

        final boolean englishSet = englishEditTextSet;
        final boolean germanSet = germanEditTextSet;



        buttonDifficulty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialogSelectFromDB(buttonDifficulty, context, DIFFICULTY);
            }
        });

        buttonSpotType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialogSelectFromDB(buttonSpotType, context, SPOT_TYPE);
            }
        });

        buttonMovementType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialogSelectFromDB(buttonMovementType, context, MOVEMENT_TYPE);
            }
        });


        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String newEnglishName = addMoveNameEnglish.getText().toString();
                final String newGermanName = addMoveNameGerman.getText().toString();

                if ((newEnglishName.isEmpty() && englishSet) || (newGermanName.isEmpty() && germanSet)) {
                    toastMessage(getResources().getString(R.string.toast_move_name_empty));
                } else {

                    Move addMove;
                    int checkDoubleNameSituation = checkDoubleMoveNames(newEnglishName, newGermanName);

                    if ((checkDoubleNameSituation == NO_DOUBLE_NAMES) || userHasBeenWarnedDoubleName) {
                        addMove = new Move(newEnglishName, newGermanName);

                        switch (currentLanguage) {
                            case "en":
                                if (!germanSet) {
                                    addMove.setGermanName(newEnglishName);
                                }
                                break;
                            case "de":
                                if (!englishSet) {
                                    addMove.setEnglishName(newGermanName);
                                }
                                break;
                            default:
                                break;
                        }

                        if (setDifficulty) {
                            addMove.setStyleOrDifficulty(buttonDifficultyState);
                            setDifficulty = false;
                        }
                        if (setSpotType) {
                            addMove.setSpotTypeNeeded(buttonSpotTypeState);
                            setSpotType = false;
                        }
                        if (setMovementType) {
                            addMove.setMovementType(buttonMovementTypeState);
                            setMovementType = false;
                        }

                        moveDao.insertAll(addMove);

                        toastMessage(getResources().getString(R.string.toast_confirm_move_creation1)
                                + getLocalisedMoveName(addMove)
                                + getResources().getString(R.string.toast_confirm_move_creation2));
                        insertItemAtBeginning(addMove);
                        updateMoveEditStatusText();
                        makeMainRecreate();
                        alertDialog.dismiss();

                    } else if (checkDoubleNameSituation == DOUBLE_ENGLISH_NAME) {
                        createDialogAddMoveDoubleWarning(newEnglishName, newGermanName, context, DOUBLE_ENGLISH_NAME);
                    } else if (checkDoubleNameSituation == DOUBLE_GERMAN_NAME) {
                        createDialogAddMoveDoubleWarning(newEnglishName, newGermanName, context, DOUBLE_GERMAN_NAME);
                    } else {
                        toastMessage(getResources().getString(R.string.toast_mild_error));
                        addMove = new Move(newEnglishName, newGermanName);

                        switch(currentLanguage) {
                            case "en":
                                if (!germanSet) {
                                    addMove.setGermanName(newEnglishName);
                                }
                                break;
                            case "de":
                                if (!englishSet) {
                                    addMove.setEnglishName(newGermanName);
                                }
                                break;
                            default:
                                break;
                        }

                        if (setDifficulty) {
                            addMove.setStyleOrDifficulty(buttonDifficultyState);
                            setDifficulty = false;
                        }
                        if (setSpotType) {
                            addMove.setSpotTypeNeeded(buttonSpotTypeState);
                            setSpotType = false;
                        }
                        if (setMovementType) {
                            addMove.setMovementType(buttonMovementTypeState);
                            setMovementType = false;
                        }

                        moveDao.insertAll(addMove);

                        toastMessage(getResources().getString(R.string.toast_confirm_move_creation1)
                                + getLocalisedMoveName(addMove)
                                + getResources().getString(R.string.toast_confirm_move_creation2));
                        insertItemAtBeginning(addMove);
                        updateMoveEditStatusText();
                        makeMainRecreate();
                        alertDialog.dismiss();
                    }

                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    private int checkDoubleMoveNames(String checkMoveNameEnglish, String checkMoveNameGerman) {
        List<Move> checkMoves = moveDao.getAll();
        for (Move a : checkMoves) {
            if (a.getEnglishName().equalsIgnoreCase(checkMoveNameEnglish)) {
                return DOUBLE_ENGLISH_NAME;
            }
            if (a.getGermanName().equalsIgnoreCase(checkMoveNameGerman)) {
                return DOUBLE_GERMAN_NAME;
            }
        }
        return NO_DOUBLE_NAMES;
    }


    private void createDialogAddMoveDoubleWarning(String doubleEnglishName, String doubleGermanName,
                                                   Context context, int errorCode) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_warning_basic, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        final TextView deleteDescr = (TextView) layout.findViewById(R.id.warning_dialog_descr);
        Button confirmButton = (Button) layout.findViewById(R.id.warning_dialog_button_confirm);
        confirmButton.setText(getResources().getString(R.string.dialog_button_ok));
        Button cancelButton = (Button) layout.findViewById(R.id.warning_dialog_button_cancel);
        cancelButton.setVisibility(View.INVISIBLE);

        final String newEnglishName = doubleEnglishName;
        final String newGermanName = doubleGermanName;

        //create message
        String message;
        String newName = "";
        switch (errorCode) {
            case DOUBLE_ENGLISH_NAME:
                newName = newEnglishName;
                break;
            case DOUBLE_GERMAN_NAME:
                newName = newGermanName;
                break;
            default:
                break;
        }

        message = getResources().getString(R.string.dialog_move_add_double_name_conflict1)
                + newName
                + getResources().getString(R.string.dialog_move_add_double_name_conflict2);

        deleteDescr.setText(message);


        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }








    private void toggleColorCodingButton() {
        int newColorCodingMoveCardsMove = (currentProfile.getColorCodingMoveCardsMode() + 1) % 4;
        currentProfile.setColorCodingMoveCardsMode(newColorCodingMoveCardsMove);
        profileDao.updateProfiles(currentProfile);

        setColorCodingButtonImage();

        recreate();
    }

    private void setColorCodingButtonImage() {
        switch (currentProfile.getColorCodingMoveCardsMode()) {
            case MOVE_CARDS_COLOR_EMPTY:
                moveEditButtonCardColorCoding.setImageResource(R.drawable.ic_empty_card_24px);
                break;
            case MOVE_CARDS_COLOR_LEFT:
                moveEditButtonCardColorCoding.setImageResource(R.drawable.ic_left_filled_card_24px);
                break;
            case MOVE_CARDS_COLOR_RIGHT:
                moveEditButtonCardColorCoding.setImageResource(R.drawable.ic_right_filled_card_24px);
                break;
            case MOVE_CARDS_COLOR_DOUBLE:
                moveEditButtonCardColorCoding.setImageResource(R.drawable.ic_double_filled_card_24px);
                break;
            default:
                break;
        }
    }

    private void toggleButtonFavorite(int movePosition) {
        Move currentFavoritePressedMove = mMoveEditList.get(movePosition).getMove();
        if (mMoveEditList.get(movePosition).getFavorite()) {
            currentFavoritePressedMove.setFavorite(false);
        } else {
            currentFavoritePressedMove.setFavorite(true);
        }
        moveDao.updateMoves(currentFavoritePressedMove);
        changedItem(movePosition, currentFavoritePressedMove);
    }


/*    private void createDialogEditMove(final int position, final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final Move editMove = mMoveEditList.get(position).getMove();
        //final int tempID = view.getId();

        boolean englishEditTextSet = false;
        boolean germanEditTextSet = false;

        String titleMessage = getResources().getString(R.string.dialog_edit_move_title1) + getLocalisedMoveName(editMove) + getResources().getString(R.string.dialog_edit_move_title2);
        builder.setTitle(titleMessage);

        final LinearLayout editTextLayout = new LinearLayout(context);
        editTextLayout.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(50, 0, 50, 0);

        //EditTexts for different Languages:
        final TextView infoTextMoveNameEnglish = new TextView(context);
        infoTextMoveNameEnglish.setText("english:");
        final EditText editMoveNameEnglish = new EditText(context);
        editMoveNameEnglish.setText(editMove.getEnglishName());

        final TextView infoTextMoveNameGerman = new TextView(context);
        infoTextMoveNameGerman.setText("german:");
        final EditText editMoveNameGerman = new EditText(context);
        editMoveNameGerman.setText(editMove.getGermanName());

        //find out which languages to edit:
        String currentLanguage = sharedPref.getString(getResources().getString(R.string.sharedpref_current_selected_language), "en");
        switch (currentLanguage) {
            case "en":
                editTextLayout.addView(editMoveNameEnglish, layoutParams);
                englishEditTextSet = true;
                break;
            case "de":
                editTextLayout.addView(editMoveNameGerman, layoutParams);
                germanEditTextSet = true;
                break;
            default:
                editTextLayout.addView(editMoveNameEnglish, layoutParams);
                break;
        }

        //TODO: test if this is even working at all, once settings are implemented far enough
        if (!englishEditTextSet && currentProfile.getEditEnglishNames()) {
            editTextLayout.addView(infoTextMoveNameEnglish);
            editTextLayout.addView(editMoveNameEnglish, layoutParams);
            englishEditTextSet = true;
        }
        if (!germanEditTextSet && currentProfile.getEditGermanNames()) {
            editTextLayout.addView(infoTextMoveNameGerman);
            editTextLayout.addView(editMoveNameGerman, layoutParams);
            germanEditTextSet = true;
        }


        TextView reminder = new TextView(context);
        reminder.setText("Remember to implement Difficulty and MoveSpotType Changer Dialog");
        editTextLayout.addView(reminder);

        builder.setView(editTextLayout);

        final boolean englishSet = englishEditTextSet;
        final boolean germanSet = germanEditTextSet;

        builder.setPositiveButton(getString(R.string.dialog_button_confirm_textedit), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                final String newEnglishName = editMoveNameEnglish.getText().toString();
                final String newGermanName = editMoveNameGerman.getText().toString();

                if ((newEnglishName.isEmpty() && englishSet) || (newGermanName.isEmpty() && germanSet)) {
                    toastMessage(getResources().getString(R.string.toast_move_name_empty));
                } else {
                    switch (checkDoubleMoveNamesExcludingSelf(editMove.getID(), newEnglishName, newGermanName)) {
                        case NO_DOUBLE_NAMES:
                            if (englishSet) {
                                editMove.setEnglishName(newEnglishName);
                            }
                            if (germanSet) {
                                editMove.setGermanName(newGermanName);
                            }
                            editMove.setChanged(true);
                            moveDao.updateMoves(editMove);

                            toastMessage(getResources().getString(R.string.toast_confirm_edit_entry));
                            changedItem(position, editMove);
                            makeMainRecreate();
                            break;
                        case DOUBLE_ENGLISH_NAME:
                            createDialogEditMoveDoubleWarning(editMove, position, newEnglishName, newGermanName, englishSet, germanSet, context, DOUBLE_ENGLISH_NAME);
                            break;
                        case DOUBLE_GERMAN_NAME:
                            createDialogEditMoveDoubleWarning(editMove, position, newEnglishName, newGermanName, englishSet, germanSet, context, DOUBLE_GERMAN_NAME);
                            break;
                        default:
                            toastMessage(getResources().getString(R.string.toast_mild_error));
                            if (englishSet) {
                                editMove.setEnglishName(newEnglishName);
                            }
                            if (germanSet) {
                                editMove.setGermanName(newGermanName);
                            }
                            editMove.setChanged(true);
                            moveDao.updateMoves(editMove);

                            toastMessage(getResources().getString(R.string.toast_confirm_edit_entry));
                            changedItem(position, editMove);
                            makeMainRecreate();
                            break;
                    }

                }
            }
        });

        builder.setNegativeButton(getString(R.string.dialog_button_cancel_normal), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        builder.show();
    }*/

    private void createDialogEditMove(final int position, final Context context) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;
        final Move editMove = mMoveEditList.get(position).getMove();

        //get all elements of the layout
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_move_edit, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        LinearLayout layoutMoveEditTexts = (LinearLayout) layout.findViewById(R.id.move_edit_dialog_linearlayout);
        TextView editTitle = (TextView) layout.findViewById(R.id.move_edit_dialog_title);
        final EditText editMoveNameEnglish = (EditText) layout.findViewById(R.id.move_edit_name_english);
        final EditText editMoveNameGerman = (EditText) layout.findViewById(R.id.move_edit_name_german);
        final Button buttonDifficulty = (Button) layout.findViewById(R.id.move_edit_dialog_button_difficulty);
        final Button buttonSpotType = (Button) layout.findViewById(R.id.move_edit_dialog_button_spot_type);
        final Button buttonMovementType = (Button) layout.findViewById(R.id.move_edit_dialog_button_movement_type);
        Button buttonSave = (Button) layout.findViewById(R.id.move_edit_dialog_button_save);
        Button buttonCancel = (Button) layout.findViewById(R.id.move_edit_dialog_button_cancel);

        editTitle.setText(getResources().getString(R.string.dialog_edit_move_title1) + getLocalisedMoveName(editMove) + getResources().getString(R.string.dialog_edit_move_title2));

        buttonDifficulty.setText(getLocalisedMoveStyleOrDifficultyDescr(editMove.getStyleOrDifficulty(), currentLanguage));
        buttonDifficultyState = editMove.getStyleOrDifficulty();
        buttonSpotType.setText(getLocalisedMoveSpotTypeNeededDescr(editMove.getSpotTypeNeeded(), currentLanguage));
        buttonSpotTypeState = editMove.getSpotTypeNeeded();
        buttonMovementType.setText(getLocalisedMovementTypeDescr(editMove.getMovementType(), currentLanguage));
        buttonMovementTypeState = editMove.getMovementType();

        boolean englishEditTextSet = false;
        boolean germanEditTextSet = false;

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(50, 0, 50, 0);

        //EditTexts for different Languages:
        editMoveNameEnglish.setText(editMove.getEnglishName());
        editMoveNameGerman.setText(editMove.getGermanName());

        //find out which languages to edit:
        switch (currentLanguage) {
            case "en":
                editMoveNameEnglish.setVisibility(View.VISIBLE);
                englishEditTextSet = true;
                break;
            case "de":
                editMoveNameGerman.setVisibility(View.VISIBLE);
                germanEditTextSet = true;
                break;
            default:
                editMoveNameEnglish.setVisibility(View.VISIBLE);
                break;
        }

        //TODO: test if this is even working at all, once settings are implemented far enough
        if (!englishEditTextSet && currentProfile.getEditEnglishNames()) {
            editMoveNameEnglish.setVisibility(View.VISIBLE);
            englishEditTextSet = true;
        }
        if (!germanEditTextSet && currentProfile.getEditGermanNames()) {
            editMoveNameGerman.setVisibility(View.VISIBLE);
            germanEditTextSet = true;
        }

        final boolean englishSet = englishEditTextSet;
        final boolean germanSet = germanEditTextSet;



        buttonDifficulty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialogSelectFromDB(buttonDifficulty, context, DIFFICULTY);
            }
        });

        buttonSpotType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialogSelectFromDB(buttonSpotType, context, SPOT_TYPE);
            }
        });

        buttonMovementType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialogSelectFromDB(buttonMovementType, context, MOVEMENT_TYPE);
            }
        });


        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String newEnglishName = editMoveNameEnglish.getText().toString();
                final String newGermanName = editMoveNameGerman.getText().toString();

                if ((newEnglishName.isEmpty() && englishSet) || (newGermanName.isEmpty() && germanSet)) {
                    toastMessage(getResources().getString(R.string.toast_move_name_empty));
                } else {
                    switch (checkDoubleMoveNamesExcludingSelf(editMove.getID(), newEnglishName, newGermanName)) {
                        case NO_DOUBLE_NAMES:
                            if (englishSet) {
                                editMove.setEnglishName(newEnglishName);
                            }
                            if (germanSet) {
                                editMove.setGermanName(newGermanName);
                            }
                            editMove.setChanged(true);

                            if (setDifficulty) {
                                editMove.setStyleOrDifficulty(buttonDifficultyState);
                                setDifficulty = false;
                            }
                            if (setSpotType) {
                                editMove.setSpotTypeNeeded(buttonSpotTypeState);
                                setSpotType = false;
                            }
                            if (setMovementType) {
                                editMove.setMovementType(buttonMovementTypeState);
                                setMovementType = false;
                            }

                            moveDao.updateMoves(editMove);

                            toastMessage(getResources().getString(R.string.toast_confirm_edit_entry));
                            changedItem(position, editMove);
                            makeMainRecreate();
                            alertDialog.dismiss();
                            break;
                        case DOUBLE_ENGLISH_NAME:
                            createDialogEditMoveDoubleWarning(alertDialog, editMove, position, newEnglishName, newGermanName, englishSet, germanSet, context, DOUBLE_ENGLISH_NAME);
                            break;
                        case DOUBLE_GERMAN_NAME:
                            createDialogEditMoveDoubleWarning(alertDialog, editMove, position, newEnglishName, newGermanName, englishSet, germanSet, context, DOUBLE_GERMAN_NAME);
                            break;
                        default:
                            toastMessage(getResources().getString(R.string.toast_mild_error));
                            if (englishSet) {
                                editMove.setEnglishName(newEnglishName);
                            }
                            if (germanSet) {
                                editMove.setGermanName(newGermanName);
                            }
                            editMove.setChanged(true);

                            if (setDifficulty) {
                                editMove.setStyleOrDifficulty(buttonDifficultyState);
                                setDifficulty = false;
                            }
                            if (setSpotType) {
                                editMove.setSpotTypeNeeded(buttonSpotTypeState);
                                setSpotType = false;
                            }
                            if (setMovementType) {
                                editMove.setMovementType(buttonMovementTypeState);
                                setMovementType = false;
                            }

                            moveDao.updateMoves(editMove);

                            toastMessage(getResources().getString(R.string.toast_confirm_edit_entry));
                            changedItem(position, editMove);
                            makeMainRecreate();
                            alertDialog.dismiss();
                            break;
                    }

                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }


    private int checkDoubleMoveNamesExcludingSelf(int excludedMoveID, String checkMoveNameEnglish, String checkMoveNameGerman) {
        List<Move> checkMoves = moveDao.getAll();
        for (Move a : checkMoves) {
            if (a.getEnglishName().equalsIgnoreCase(checkMoveNameEnglish)) {
                if (a.getID() != excludedMoveID) {
                    return DOUBLE_ENGLISH_NAME;
                }
            }
            if (a.getGermanName().equalsIgnoreCase(checkMoveNameGerman)) {
                if (a.getID() != excludedMoveID) {
                    return DOUBLE_GERMAN_NAME;
                }
            }
        }
        return NO_DOUBLE_NAMES;
    }

/*    private void createDialogEditMoveDoubleWarning(final Move toBeChangedMove, final int position,
                                                   String doubleEnglishName, String doubleGermanName,
                                                   final boolean englishSet, final boolean germanSet,
                                                   Context context, int errorCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final String newEnglishName = doubleEnglishName;
        final String newGermanName = doubleGermanName;

        builder.setTitle(getString(R.string.dialog_warning_title));
        String message;
        String toBeChangedName = "";
        String newName = "";
        switch (errorCode) {
            case DOUBLE_ENGLISH_NAME:
                toBeChangedName = toBeChangedMove.getEnglishName();
                newName = newEnglishName;
                break;
            case DOUBLE_GERMAN_NAME:
                toBeChangedName = toBeChangedMove.getGermanName();
                newName = newGermanName;
                break;
            default:
                break;
        }

        message = getResources().getString(R.string.dialog_move_edit_double_name_conflict_1)
                + toBeChangedName
                + getResources().getString(R.string.dialog_move_edit_double_name_conflict_2)
                + newName
                + getResources().getString(R.string.dialog_move_edit_double_name_conflict_3);


        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.dialog_button_confirm_warning), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //TODO: set other languages names to inputName, change move.changed state to true after edit.
                if (englishSet) {
                    toBeChangedMove.setEnglishName(newEnglishName);
                }
                if (germanSet) {
                    toBeChangedMove.setGermanName(newGermanName);
                }
                toBeChangedMove.setChanged(true);
                moveDao.updateMoves(toBeChangedMove);

                toastMessage(getResources().getString(R.string.toast_confirm_edit_entry));
                changedItem(position, toBeChangedMove);
                makeMainRecreate();
            }
        });
        builder.setNegativeButton(getString(R.string.dialog_button_cancel_warning), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                toastMessage(getResources().getString(R.string.toast_discard_edit_entry));
            }
        });
        builder.show();
    }*/




    private void createDialogEditMoveDoubleWarning(final android.app.AlertDialog alertDialogIn, final Move toBeChangedMove, final int position,
                                                   String doubleEnglishName, String doubleGermanName,
                                                   final boolean englishSet, final boolean germanSet,
                                                   Context context, int errorCode) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_warning_basic, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        final TextView deleteTitle = (TextView) layout.findViewById(R.id.warning_dialog_title);
        final TextView deleteDescr = (TextView) layout.findViewById(R.id.warning_dialog_descr);
        Button confirmButton = (Button) layout.findViewById(R.id.warning_dialog_button_confirm);
        Button cancelButton = (Button) layout.findViewById(R.id.warning_dialog_button_cancel);

        final String newEnglishName = doubleEnglishName;
        final String newGermanName = doubleGermanName;

        //create message
        String message;
        String toBeChangedName = "";
        String newName = "";
        switch (errorCode) {
            case DOUBLE_ENGLISH_NAME:
                toBeChangedName = toBeChangedMove.getEnglishName();
                newName = newEnglishName;
                break;
            case DOUBLE_GERMAN_NAME:
                toBeChangedName = toBeChangedMove.getGermanName();
                newName = newGermanName;
                break;
            default:
                break;
        }

        message = getResources().getString(R.string.dialog_move_edit_double_name_conflict_1)
                + toBeChangedName
                + getResources().getString(R.string.dialog_move_edit_double_name_conflict_2)
                + newName
                + getResources().getString(R.string.dialog_move_edit_double_name_conflict_3);

        deleteDescr.setText(message);


        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: set other languages names to inputName, change move.changed state to true after edit.
                if (englishSet) {
                    toBeChangedMove.setEnglishName(newEnglishName);
                }
                if (germanSet) {
                    toBeChangedMove.setGermanName(newGermanName);
                }
                toBeChangedMove.setChanged(true);
                moveDao.updateMoves(toBeChangedMove);

                toastMessage(getResources().getString(R.string.toast_confirm_edit_entry));
                changedItem(position, toBeChangedMove);
                makeMainRecreate();
                alertDialog.dismiss();
                alertDialogIn.dismiss();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }



    public void createDialogSelectFromDB(final Button buttonIn, Context context, int requestedDB) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_select_from_db, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        LinearLayout layoutSpotCopy = (LinearLayout) layout.findViewById(R.id.select_from_db_dialog_linearlayout);
        TextView dialogTitle = (TextView) layout.findViewById(R.id.select_from_db_dialog_title);
        Button buttonCancel = (Button) layout.findViewById(R.id.select_from_db_dialog_button_cancel);

        //list all DB entries
        switch(requestedDB) {
            case DIFFICULTY:
                dialogTitle.setText(getResources().getString(R.string.dialog_button_set_difficulty));
                List<MoveStyleOrDifficulty> allDifficulties = moveStyleOrDifficultyDao.getAll();
                for (int i = 0; i < allDifficulties.size(); i++) {
                    final MoveStyleOrDifficulty nextDifficulty = allDifficulties.get(i);
                    TextView nextDifficultyView;
                    final String nextDifficultyLocalisedName = getLocalisedMoveStyleOrDifficultyDescr(nextDifficulty.getID(), currentLanguage);
                    nextDifficultyView = createDBSelectTextViewFromString(nextDifficultyLocalisedName);
                    nextDifficultyView.setClickable(true);
                    nextDifficultyView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            buttonIn.setText(nextDifficultyLocalisedName);
                            buttonDifficultyState = nextDifficulty.getID();
                            setDifficulty = true;
                            alertDialog.dismiss();
                            //toastMessageShort(nextSpot.getName() + "" +  nextSpot.getID() + "" + nextSpot.getProfileID());
                        }
                    });
                    layoutSpotCopy.addView(nextDifficultyView);
                }
                break;

            case SPOT_TYPE:
                dialogTitle.setText(getResources().getString(R.string.dialog_button_set_spot_type));
                List<MoveSpotTypeNeeded> allSpotTypes = moveSpotTypeNeededDao.getAll();
                for (int i = 0; i < allSpotTypes.size(); i++) {
                    final MoveSpotTypeNeeded nextSpotType = allSpotTypes.get(i);
                    TextView nextSpotTypeView;
                    final String nextSpotTypeLocalisedName = getLocalisedMoveSpotTypeNeededDescr(nextSpotType.getID(), currentLanguage);
                    nextSpotTypeView = createDBSelectTextViewFromString(nextSpotTypeLocalisedName);
                    nextSpotTypeView.setClickable(true);
                    nextSpotTypeView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            buttonIn.setText(nextSpotTypeLocalisedName);
                            buttonSpotTypeState = nextSpotType.getID();
                            setSpotType = true;
                            alertDialog.dismiss();
                            //toastMessageShort(nextSpot.getName() + "" +  nextSpot.getID() + "" + nextSpot.getProfileID());
                        }
                    });
                    layoutSpotCopy.addView(nextSpotTypeView);
                }
                break;

            case MOVEMENT_TYPE:
                dialogTitle.setText(getResources().getString(R.string.dialog_button_set_movement_type));
                List<MovementType> allMovementTypes = movementTypeDao.getAll();
                for (int i = 0; i < allMovementTypes.size(); i++) {
                    final MovementType nextMovementType = allMovementTypes.get(i);
                    TextView nextMovementTypeView;
                    final String nextMovementTypeLocalisedName = getLocalisedMovementTypeDescr(nextMovementType.getID(), currentLanguage);
                    nextMovementTypeView = createDBSelectTextViewFromString(nextMovementTypeLocalisedName);
                    nextMovementTypeView.setClickable(true);
                    nextMovementTypeView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            buttonIn.setText(nextMovementTypeLocalisedName);
                            buttonMovementTypeState = nextMovementType.getID();
                            setMovementType = true;
                            alertDialog.dismiss();
                            //toastMessageShort(nextSpot.getName() + "" +  nextSpot.getID() + "" + nextSpot.getProfileID());
                        }
                    });
                    layoutSpotCopy.addView(nextMovementTypeView);
                }
                break;
        }


        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }


    private TextView createDBSelectTextViewFromString(String string) {
        TextView child = new TextView(MoveEditActivity.this);
        //child.setWidth(layout_viewCombo.getWidth());
        child.setText(string);
            child.setPadding(0, 0, 0, 0);
            child.setTypeface(Typeface.DEFAULT_BOLD);
            child.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    getResources().getDimension(R.dimen.scrollview_title_big));
        return child;
    }








    public void createDialogToggleMoveInSpots(final int position, Context context) {
    //TODO
        toastMessageShort("implement");
    }


/*    public void createDialogArchiveMove(final int position, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final String tempMoveNameLocalised = getLocalisedMoveName(mMoveEditList.get(position).getMove());
        //final Profile tempProfile = profileDao.findByID(tempID);
        builder.setTitle(getString(R.string.dialog_move_edit_archive_title));
        String message = getResources().getString(R.string.dialog_move_edit_archive_text1) + tempMoveNameLocalised + getResources().getString(R.string.dialog_move_edit_archive_text2);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.dialog_button_confirm_warning), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                archiveItem(position);
                toastMessageShort(tempMoveNameLocalised + " " + getResources().getString(R.string.toast_move_edit_archived_move));
            }
        });
        builder.setNegativeButton(getString(R.string.dialog_button_cancel_warning), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        builder.show();
    }*/

    private void createDialogArchiveMove(final int position, Context context){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;
        final String tempMoveNameLocalised = getLocalisedMoveName(mMoveEditList.get(position).getMove());

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_warning_basic, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        final TextView archiveTitle = (TextView) layout.findViewById(R.id.warning_dialog_title);
        final TextView archiveDescr = (TextView) layout.findViewById(R.id.warning_dialog_descr);
        Button confirmButton = (Button) layout.findViewById(R.id.warning_dialog_button_confirm);
        Button cancelButton = (Button) layout.findViewById(R.id.warning_dialog_button_cancel);

        archiveTitle.setText(getString(R.string.dialog_move_edit_archive_title));

        String message = getResources().getString(R.string.dialog_move_edit_archive_text1) + tempMoveNameLocalised + getResources().getString(R.string.dialog_move_edit_archive_text2);
        archiveDescr.setText(message);

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                archiveItem(position);
                toastMessageShort(tempMoveNameLocalised + " " + getResources().getString(R.string.toast_move_edit_archived_move));
                alertDialog.dismiss();

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }


/*    public void createDialogDeleteMove(final int position, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final String tempMoveNameLocalised = getLocalisedMoveName(mMoveEditList.get(position).getMove());
        //final Profile tempProfile = profileDao.findByID(tempID);
        builder.setTitle(getString(R.string.dialog_warning_title));
        builder.setMessage(getResources().getString(R.string.dialog_delete_entry) + " " + tempMoveNameLocalised);
        builder.setPositiveButton(getString(R.string.dialog_button_confirm_warning), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                removeItemAndDeleteFromDB(position);
                toastMessage(getString(R.string.toast_confirm_delete_entry));
            }
        });
        builder.setNegativeButton(getString(R.string.dialog_button_cancel_warning), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        builder.show();
    }*/

    private void createDialogDeleteMove(final int position, Context context){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        final android.app.AlertDialog alertDialog;
        final String tempMoveNameLocalised = getLocalisedMoveName(mMoveEditList.get(position).getMove());

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_warning_basic, (ViewGroup) findViewById(R.id.layout_root));

        builder.setView(layout);
        alertDialog = builder.create();

        final TextView deleteTitle = (TextView) layout.findViewById(R.id.warning_dialog_title);
        final TextView deleteDescr = (TextView) layout.findViewById(R.id.warning_dialog_descr);
        Button confirmButton = (Button) layout.findViewById(R.id.warning_dialog_button_confirm);
        Button cancelButton = (Button) layout.findViewById(R.id.warning_dialog_button_cancel);

        deleteDescr.setText(getResources().getString(R.string.dialog_delete_entry) + " " + tempMoveNameLocalised);

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeItemAndDeleteFromDB(position);
                toastMessage(getString(R.string.toast_confirm_delete_entry));
                alertDialog.dismiss();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }













/*    public void chooseItem(int position) {
        if (mMoveEditList.get(position).getMoveID() != currentMove.getID()) {
            currentProfile.setCurrentMoveId(mMoveEditList.get(position).getMoveID());
            profileDao.updateProfiles(currentProfile);
            updateMoveEditStatusText();
            makeMainRecreate();
            finish();
        }
    }*/

    public void insertItem(int position, Move newMove) {
        mMoveEditList.add(position, new MoveEditItem(newMove));
        mAdapter.notifyItemInserted(position);
        mRecyclerView.scrollToPosition(position);
        entryHasBeenChanged = true;
    }

    public void insertItemAtEnd(Move newMove) {
        insertItem(mMoveEditList.size(), newMove);
    }

    public void insertItemAtBeginning(Move newMove) {
        insertItem(0, newMove);
    }

    public void removeItemAndDeleteFromDB(int position) {
        //just pass the position, the move gets deleted here:
        boolean lastMoveDeleted = (mMoveEditList.size() <= 1);

        moveDao.delete(mMoveEditList.get(position).getMove());

        mMoveEditList.remove(position);
        mAdapter.notifyItemRemoved(position);

        if (lastMoveDeleted) {
            recreate();
            toastMessage(getResources().getString(R.string.move_edit_judgement_deleted_last_move));
        } else {
            updateMoveEditStatusText();
        }
    }

    public void archiveItem(int position) {
        boolean lastMoveArchived = (mMoveEditList.size() <= 1);

        Move archiveMove = mMoveEditList.get(position).getMove();
        archiveMove.setArchived(true);
        moveDao.updateMoves(archiveMove);

        mMoveEditList.remove(position);
        mAdapter.notifyItemRemoved(position);

        if (lastMoveArchived) {
            recreate();
            toastMessage(getResources().getString(R.string.move_edit_judgement_archived_last_move));
        } else {
            updateMoveEditStatusText();
        }
    }

    public void changedItem(int position, Move changedMove) {
        //changedMove should already have been updated into the Database, this only handles the change of the RecyclerView
        mMoveEditList.get(position).updateMove(changedMove);
        mAdapter.notifyItemChanged(position);
        entryHasBeenChanged = true;
    }


    private void sortMoveList() {
        switch (currentProfile.getSortByMoveEditMode()) {
            case SORT_BY_DATE:
                if (currentProfile.getSortMoveEditAscending()) {
                    Collections.sort(mMoveEditList, MoveEditItem.BY_DATE_ASCENDING);
                } else {
                    Collections.sort(mMoveEditList, MoveEditItem.BY_DATE_DESCENDING);
                }
                break;
            case SORT_BY_NAME:
                if (currentProfile.getSortMoveEditAscending()) {
                    switch (sharedPref.getString(getResources().getString(R.string.sharedpref_current_selected_language), "en")) {
                        case "en":
                            Collections.sort(mMoveEditList, MoveEditItem.BY_ENGLISH_NAME_ASCENDING);
                            break;
                        case "de":
                            Collections.sort(mMoveEditList, MoveEditItem.BY_GERMAN_NAME_ASCENDING);
                        default:
                            Collections.sort(mMoveEditList, MoveEditItem.BY_ENGLISH_NAME_ASCENDING);
                            break;
                    }
                } else {
                    switch (sharedPref.getString(getResources().getString(R.string.sharedpref_current_selected_language), "en")) {
                        case "en":
                            Collections.sort(mMoveEditList, MoveEditItem.BY_ENGLISH_NAME_DESCENDING);
                            break;
                        case "de":
                            Collections.sort(mMoveEditList, MoveEditItem.BY_GERMAN_NAME_DESCENDING);
                            break;
                        default:
                            Collections.sort(mMoveEditList, MoveEditItem.BY_ENGLISH_NAME_DESCENDING);
                            break;
                    }
                }
                break;
            case SORT_BY_FAVORITE:
                if (currentProfile.getSortMoveEditAscending()) {
                    Collections.sort(mMoveEditList, MoveEditItem.BY_FAVORITE_ASCENDING);
                } else {
                    Collections.sort(mMoveEditList, MoveEditItem.BY_FAVORITE_DESCENDING);
                }
                break;
            case SORT_BY_DIFFICULTY:
                if (currentProfile.getSortMoveEditAscending()) {
                    Collections.sort(mMoveEditList, MoveEditItem.BY_DIFFICULTY_ASCENDING);
                } else {
                    Collections.sort(mMoveEditList, MoveEditItem.BY_DIFFICULTY_DESCENDING);
                }
                break;
            case SORT_BY_SPOTTYPE:
                if (currentProfile.getSortMoveEditAscending()) {
                    Collections.sort(mMoveEditList, MoveEditItem.BY_SPOT_TYPE_ASCENDING);
                } else {
                    Collections.sort(mMoveEditList, MoveEditItem.BY_SPOT_TYPE_DESCENDING);
                }
                break;
            default:
                if (currentProfile.getSortMoveEditAscending()) {
                    Collections.sort(mMoveEditList, MoveEditItem.BY_DATE_ASCENDING);
                } else {
                    Collections.sort(mMoveEditList, MoveEditItem.BY_DATE_DESCENDING);
                }
                break;
        }
    }

    private void updateSortIcon() {
        switch (currentProfile.getSortByMoveEditMode()) {
            case SORT_BY_DATE:
                if (currentProfile.getSortMoveEditAscending()) {
                    setSortIcon(R.drawable.ic_baseline_sorted_24px1234);
                } else {
                    setSortIcon(R.drawable.ic_baseline_sorted_24px4321);
                }
                break;
            case SORT_BY_NAME:
                if (currentProfile.getSortMoveEditAscending()) {
                    setSortIcon(R.drawable.ic_baseline_sorted_24px1324);
                } else {
                    setSortIcon(R.drawable.ic_baseline_sorted_24px4231);
                }
                break;
            case SORT_BY_FAVORITE:
                if (currentProfile.getSortMoveEditAscending()) {
                    setSortIcon(R.drawable.ic_baseline_sorted_24px1423);
                } else {
                    setSortIcon(R.drawable.ic_baseline_sorted_24px3241);
                }
                break;
            case SORT_BY_DIFFICULTY:
                if (currentProfile.getSortMoveEditAscending()) {
                    setSortIcon(R.drawable.ic_baseline_sorted_24px1243);
                } else {
                    setSortIcon(R.drawable.ic_baseline_sorted_24px3421);
                }
                break;
            case SORT_BY_SPOTTYPE:
                if (currentProfile.getSortMoveEditAscending()) {
                    setSortIcon(R.drawable.ic_baseline_sorted_24px1342);
                } else {
                    setSortIcon(R.drawable.ic_baseline_sorted_24px2431);
                }
                break;
            default:
                if (currentProfile.getSortMoveEditAscending()) {
                    setSortIcon(R.drawable.ic_baseline_sorted_24px1432);
                } else {
                    setSortIcon(R.drawable.ic_baseline_sorted_24px2341);
                }
                break;
        }
    }

    private void setSortIcon(int id) {
        try {
            menu.findItem(R.id.action_move_edit_sort).setIcon(ContextCompat.getDrawable(this, id));
        } catch (NullPointerException e) {

        }
    }


    private void updateMoveEditStatusText() {
        String statusMessage;
        allActiveMoves = moveDao.getAllActive();
        if (allActiveMoves.size() < 1) {
            statusMessage = getResources().getString(R.string.move_edit_status_no_existing_moves);
        } else {
            List<Move> allArchivedMoves = moveDao.getAllArchived();
            if (allArchivedMoves.size() > 0) {
                statusMessage = getResources().getString(R.string.move_edit_status_title_active_amount) + " " + String.valueOf(allActiveMoves.size());
                statusMessage = statusMessage + "\n" + getResources().getString(R.string.move_edit_status_title_archived_amount) + " " + String.valueOf(allArchivedMoves.size());
            } else {
                statusMessage = getResources().getString(R.string.move_edit_status_title) + " " + String.valueOf(allActiveMoves.size());
            }
        }
        moveEditStatusText.setText(statusMessage);
    }

}
