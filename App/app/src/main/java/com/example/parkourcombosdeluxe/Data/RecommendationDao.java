package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface RecommendationDao {

    @Query("SELECT * FROM recommendation")
    List<Recommendation> getAll();

    @Query("SELECT * FROM recommendation where ID LIKE  :ID")
    Recommendation findByID(Integer ID);

    @Insert
    void insertAll(Recommendation... recommendations);

    @Update
    void updateRecommendations(Recommendation... recommendations);

    @Delete
    void delete(Recommendation recommendation);

    @Query("DELETE FROM recommendation")
    void deleteAll();
}
