package com.example.parkourcombosdeluxe.Helpers;

import android.content.Context;
import android.widget.Toast;

public final class ToastHelper {
    public static void toastMessage(Context context, String message) {
        Toast.makeText(context , message, Toast.LENGTH_LONG).show();
    }

    public static void toastMessageShort(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
