package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface SpotDao {

    @Query("SELECT * FROM spot")
    List<Spot> getAll();

    @Query("SELECT * FROM spot where profileID LIKE  :profileID")
    List<Spot> getAllByProfileID(Integer profileID);

    @Query("SELECT * FROM spot where ID LIKE  :ID")
    Spot findByID(Integer ID);

    @Insert
    void insertAll(Spot... spots);

    @Update
    void updateSpots(Spot... spots);

    @Delete
    void delete(Spot spot);

    @Query("DELETE FROM spot")
    void deleteAll();
}