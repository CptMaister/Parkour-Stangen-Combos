package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface TippDao {

    @Query("SELECT * FROM tipp")
    List<Tipp> getAll();

    @Query("SELECT * FROM tipp where ID LIKE  :ID")
    Tipp findByID(Integer ID);

    @Insert
    void insertAll(Tipp... tipps);

    @Update
    void updateTipps(Tipp... tipps);

    @Delete
    void delete(Tipp tipp);

    @Query("DELETE FROM tipp")
    void deleteAll();
}
