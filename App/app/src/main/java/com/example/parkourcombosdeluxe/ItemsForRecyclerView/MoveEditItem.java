package com.example.parkourcombosdeluxe.ItemsForRecyclerView;

import com.example.parkourcombosdeluxe.Data.Move;

import java.util.Comparator;

public class MoveEditItem {

    private Move mNextMove;
    private Boolean mActionModeChecked = false;
    //private int mImageResource;
    //private String mTextTitle;
    //private String mTextDescr;

    public MoveEditItem(Move move) {
        mNextMove = move;
        //mImageResource = imageResource;
        //mTextTitle = move.getName();
        //mTextDescr = String.valueOf(move.getCombosDoneCount());
    }

    public void updateMove(Move updatedMove) {
        mNextMove = updatedMove;
    }

    public Move getMove() {
        return mNextMove;
    }

    public int getMoveID() {
        return mNextMove.getID();
    }

    public String getEnglishName() {
        return mNextMove.getEnglishName();
    }

    public String getGermanName() {
        return mNextMove.getGermanName();
    }

    public int getStyleOrDifficulty() {
        return mNextMove.getStyleOrDifficulty();
    }

    public int getSpotTypeNeeded() {
        return mNextMove.getSpotTypeNeeded();
    }

    public Boolean getFavorite() {
        return mNextMove.getFavorite();
    }

    public Boolean getArchived() {
        return mNextMove.getArchived();
    }

    public Boolean getChanged() {
        return mNextMove.getChanged();
    }

    public String getIconColor() {
        return mNextMove.getIconColor();
    }

    public Boolean getActionModeChecked() {
        return mActionModeChecked;
    }

    public void setActionModeChecked(Boolean bool) {
        mActionModeChecked = bool;
    }


    public static final Comparator<MoveEditItem> BY_DATE_ASCENDING = new Comparator<MoveEditItem>() {
        @Override
        public int compare(MoveEditItem o1, MoveEditItem o2) {
            return o2.getMoveID() - o1.getMoveID();
        }
    };

    public static final Comparator<MoveEditItem> BY_ENGLISH_NAME_ASCENDING = new Comparator<MoveEditItem>() {
        @Override
        public int compare(MoveEditItem o1, MoveEditItem o2) {
            return o1.getEnglishName().compareTo(o2.getEnglishName());
        }
    };

    public static final Comparator<MoveEditItem> BY_GERMAN_NAME_ASCENDING = new Comparator<MoveEditItem>() {
        @Override
        public int compare(MoveEditItem o1, MoveEditItem o2) {
            return o1.getGermanName().compareTo(o2.getGermanName());
        }
    };

    public static final Comparator<MoveEditItem> BY_FAVORITE_ASCENDING = new Comparator<MoveEditItem>() {
        @Override
        public int compare(MoveEditItem o1, MoveEditItem o2) {
            return o1.getFavorite().compareTo(o2.getFavorite());
        }
    };

    public static final Comparator<MoveEditItem> BY_DIFFICULTY_ASCENDING = new Comparator<MoveEditItem>() {
        @Override
        public int compare(MoveEditItem o1, MoveEditItem o2) {
            return o1.getStyleOrDifficulty() - o2.getStyleOrDifficulty();
        }
    };

    public static final Comparator<MoveEditItem> BY_SPOT_TYPE_ASCENDING = new Comparator<MoveEditItem>() {
        @Override
        public int compare(MoveEditItem o1, MoveEditItem o2) {
            return o1.getSpotTypeNeeded() - o2.getSpotTypeNeeded();
        }
    };




    public static final Comparator<MoveEditItem> BY_DATE_DESCENDING = new Comparator<MoveEditItem>() {
        @Override
        public int compare(MoveEditItem o1, MoveEditItem o2) {
            //TODO: check if all of this si ascending at all
            return o1.getMoveID() - o2.getMoveID();
        }
    };

    public static final Comparator<MoveEditItem> BY_ENGLISH_NAME_DESCENDING = new Comparator<MoveEditItem>() {
        @Override
        public int compare(MoveEditItem o1, MoveEditItem o2) {
            return o2.getEnglishName().compareTo(o1.getEnglishName());
        }
    };

    public static final Comparator<MoveEditItem> BY_GERMAN_NAME_DESCENDING = new Comparator<MoveEditItem>() {
        @Override
        public int compare(MoveEditItem o1, MoveEditItem o2) {
            return o2.getGermanName().compareTo(o1.getGermanName());
        }
    };

    public static final Comparator<MoveEditItem> BY_FAVORITE_DESCENDING = new Comparator<MoveEditItem>() {
        @Override
        public int compare(MoveEditItem o1, MoveEditItem o2) {
            return o2.getFavorite().compareTo(o1.getFavorite());
        }
    };

    public static final Comparator<MoveEditItem> BY_DIFFICULTY_DESCENDING = new Comparator<MoveEditItem>() {
        @Override
        public int compare(MoveEditItem o1, MoveEditItem o2) {
            return o2.getStyleOrDifficulty() - o1.getStyleOrDifficulty();
        }
    };

    public static final Comparator<MoveEditItem> BY_SPOT_TYPE_DESCENDING = new Comparator<MoveEditItem>() {
        @Override
        public int compare(MoveEditItem o1, MoveEditItem o2) {
            return o2.getSpotTypeNeeded() - o1.getSpotTypeNeeded();
        }
    };





}
