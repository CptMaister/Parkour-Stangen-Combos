package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface MoveStyleOrDifficultyDao {

    @Query("SELECT * FROM moveStyleOrDifficulty")
    List<MoveStyleOrDifficulty> getAll();

    @Query("SELECT * FROM moveStyleOrDifficulty where ID LIKE  :ID")
    MoveStyleOrDifficulty findByID(Integer ID);

    @Insert
    void insertAll(MoveStyleOrDifficulty... moveStyleOrDifficultys);

    @Update
    void updateMoveStyleOrDifficultys(MoveStyleOrDifficulty... moveStyleOrDifficultys);

    @Delete
    void delete(MoveStyleOrDifficulty moveStyleOrDifficulty);

    @Query("DELETE FROM moveStyleOrDifficulty")
    void deleteAll();
}
