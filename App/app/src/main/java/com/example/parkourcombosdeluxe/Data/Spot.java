package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Spot {
    @PrimaryKey(autoGenerate = true)
    public Integer ID;
    @NonNull
    public Integer profileID;
    public String name;
    public Integer combosDoneCount;


    public Spot(Integer profileID, String name, Integer combosDoneCount) {
        this.profileID = profileID;
        this.name = name;
        this.combosDoneCount = combosDoneCount;
    }

    //standard Constructor
    @Ignore
    public Spot(@NonNull Integer profileID, String name) {
        this(profileID, name, 0);
    }

    @Ignore
    public Spot(@NonNull Integer profileID) {
        this(profileID, "Spot", 0);
    }

    public Integer getID() {
        return ID;
    }
    public void setID(Integer ID) {
        this.ID = ID;
    }

    @NonNull
    public Integer getProfileID() {
        return profileID;
    }
    public void setProfileID(@NonNull Integer profileID) {
        this.profileID = profileID;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Integer getCombosDoneCount() {
        return combosDoneCount;
    }
    public void setCombosDoneCount(Integer combosDoneCount) {
        this.combosDoneCount = combosDoneCount;
    }
    public void addToCombosDoneCount(Integer add) {
        this.combosDoneCount = this.combosDoneCount + add;
    }

}