package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface MoveSpotTypeNeededDao {

    @Query("SELECT * FROM moveSpotTypeNeeded")
    List<MoveSpotTypeNeeded> getAll();

    @Query("SELECT * FROM moveSpotTypeNeeded where ID LIKE  :ID")
    MoveSpotTypeNeeded findByID(Integer ID);

    @Insert
    void insertAll(MoveSpotTypeNeeded... moveSpotTypeNeededs);

    @Update
    void updateMoveSpotTypeNeededs(MoveSpotTypeNeeded... moveSpotTypeNeededs);

    @Delete
    void delete(MoveSpotTypeNeeded moveSpotTypeNeeded);

    @Query("DELETE FROM moveSpotTypeNeeded")
    void deleteAll();
}
