package com.example.parkourcombosdeluxe.Data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;


@Dao
public interface MoveDao {

    @Query("SELECT * FROM move")
    List<Move> getAll();

    @Query("SELECT * FROM move where archived = 0")
    List<Move> getAllActive();

    @Query("SELECT * FROM move where archived = 1")
    List<Move> getAllArchived();

    @Query("SELECT ID, englishName, germanName, favorite FROM move")
    List<MoveMinimal> getAllMinimal();

    @Query("SELECT * FROM move where ID LIKE  :ID")
    Move findByID(Integer ID);

    @Insert
    void insertAll(Move... moves);

    @Update
    void updateMoves(Move... moves);

    @Delete
    void delete(Move move);

    @Query("DELETE FROM move")
    void deleteAll();
}


/*    @Query("SELECT * FROM user where first_name LIKE  :firstName AND last_name LIKE :lastName")
    User findByName(String firstName, String lastName);

    @Query("SELECT COUNT(*) from user")
    int countUsers();*/
